export interface IProduct {
    id : number,
    name : string,
    image : string
    decription: string
}
export const initproduct = {
     id : 0,
     name: "",
     image: "img/prod-5.jpg",
     decription: ""   
} as IProduct