import { IProduct } from "~/types";
const products = [
    {
        id : 1,
        name : "giày 1",
        image : "img/prod-5.jpg",
        decription: "size 1"
    },
    {
        id : 2,
        name : "giày 2 ",
        image : "img/photo2.png",
        decription: "size 1"
    },
    {
        id : 3,
        name : "giày 3",
        image : "img/prod-5.jpg",
        decription: "size 1"
    },
    {
        id : 4,
        name : "giày 4",
        image : "img/photo4.jpg",
        decription: "size 1"
    },
] as IProduct[] ;
export default products;
